const Discord = require("discord.js")
const bot = new Discord.Client()
const hentai_patterns = {
    nhentai: {
        pattern: /\d{6}/,
        url: "https://nhentai.net/g/"
    },
    tsumino: {
        pattern: /\d{5}/,
        url: "https://www.tsumino.com/Book/Info/"
    }
    
}
const blacklisted_patterns = [/<\:.*>/, /<\a\:.*>/, /<@.*>/, /<#.*>/, /http\:\/\/.*/, /https\:\/\/.*/, /.*www\..*/]

bot.on('message', message => {
    console.log(message.content)
    blacklisted = false;

    // keeps the bot from replying to himself
    if (message.author.bot) return;

    // denies the bot from sending messages from known blacklist patterns
    blacklisted_patterns.forEach(function(pattern)
    {
        if (message.content.match(pattern)) blacklisted = true;
    }) 

    if (blacklisted) return;

    for (var site in hentai_patterns)
    {
        //if the message sent matches a known site pattern, sends it
        if (message.content.match(hentai_patterns[site].pattern))
        {
            //write message with sample url
            message.channel.send(('<' + hentai_patterns[site].url + message.content.match(hentai_patterns[site].pattern) + '>').replace(/\s/g, ''))
            break;
        }
    }
})

bot.on('voiceStateUpdate', function (oldMember, newMember) {
	// Gives user the voice role if they entered the proper channel
	//Change voice role accordingly to the proper channel id (find it with \@role, the role has to be pingable (sorry about that solution i'll find a cleaner way to do it in the future dw uwu))
	const voiceRole = 'voice role id goes here'	

	// Checks if the user is inside the channel, else remove the role anyway (could be improved)
	if(newMember.voiceChannelID !== null)
	{
		newMember.addRole(voiceRole, `${newMember.username} joined voice chat`)
			.then(console.log)
			.catch(console.error)
	}
	else
	{
		newMember.removeRole(voiceRole, `${newMember.username} left voice chat`)
			.then(console.log)
			.catch(console.error)
	}
})

bot.on('ready', () => {
    console.log(`Logged in as ${bot.user.tag}!`)
})

// Change token accordingly to the bot you're using
bot.login('token goes here')
